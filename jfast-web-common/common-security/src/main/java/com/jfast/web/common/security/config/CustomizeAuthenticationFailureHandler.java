package com.jfast.web.common.security.config;

import com.alibaba.fastjson.JSONObject;
import com.jfast.web.common.core.utils.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录失败处理
 * @author zengjintao
 * @version 1.0
 * @create_at 2019/10/20 13:45
 */
@Component
@Slf4j
public class CustomizeAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {
        log.error("登录失败", exception);
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().write(JSONObject.toJSONString(new ResultCode(ResultCode.FAIL, "登录失败")));
    }
}
