package com.jfast.web.common.core.model;

import com.jfast.web.common.core.bean.ModelBean;

/**
 * 管理员实体类
 * @author zengjintao
 * @version 1.0
 * @create_at 2020/9/24 14:34
 */
public class SystemAdmin extends ModelBean<SystemAdmin> {
}
