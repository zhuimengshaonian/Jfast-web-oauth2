package com.jfast.web.common.core.constants;

/**
 * @author zengjintao
 * @version 1.0
 * @create_at 2019/12/24 18:59
 */
public class Constants {

    public static final String IMAGE_CODE = "image_code";
    public static final String OAUTH2_LOGGIN_URL = "/auth/oauth/token";

    public static final String JFAST_WEB_API_ADMIN_SERVER = "jfast-web-api-admin";
}
