package com.jfast.web.api.admin.controller;

import com.jfast.web.common.core.base.BaseController;
import org.springframework.web.bind.annotation.RestController;

/**
 * 菜单管理
 * @author zengjintao
 * @version 1.0
 * @create_at 2019/12/29 19:28
 */
@RestController("/menu")
public class MenuController extends BaseController {
}
