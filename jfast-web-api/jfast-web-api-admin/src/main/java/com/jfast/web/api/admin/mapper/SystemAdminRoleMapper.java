package com.jfast.web.api.admin.mapper;

import com.jfast.web.common.core.base.BaseMapper;

/**
 * 管理员角色关联Mapper映射
 * @author zengjintao
 * @version 1.0
 * @create_at 2019/12/28 11:21
 */
public interface SystemAdminRoleMapper extends BaseMapper {
}
